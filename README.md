# Power Globes

**Compatibility:** Classic Doom source ports supporting ACS and DECORATE.

Its a prop set which, after destroyed, gives a debuff for every player and monster in the map. The debuffs are as follows:

  - Gravity Shock: greatly reduces the gravity of the map
  - Environmental Pain: damages both players and enemies periodically, forcing pain on players and non-boss enemies

These effects last until the map is changed.

It's also coded an Environmental Painkiller item, which allows players to stop suffering damage and pain from Environmental Pain Power Globe while it's in their inventory.

Lore: these globes are responsible for maintaining atmosphere stability. Once destroyed, every inhabitant of the region (being on a planet, moon or installation, for instance) will suffer with a discomfort, reducing their life expectancy and labor effectiveness, until restored.
